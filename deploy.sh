#!/usr/bin/env bash

# Create public folder
mkdir -p public/czech public/english 2> /dev/null

# Set nullglob
shopt -s nullglob

# Functions
adoc2html() {
    input_file="$1"
    output_file="public/${input_file%%.adoc}.html"
    filename="$(basename $file)"

    echo -n Converting file: \'"$input_file"\'

    asciidoctor -o "$output_file" "$input_file"

    echo ' [DONE]'
}

# Convert all articles
for file in {czech,english}/*.adoc; do
    adoc2html "$file"
done

# Copy images
for dir in {czech,english}/*; do
    [ -d "$dir" ] || continue

    echo -n Copying directory: \'"$dir"\'

    cp -r "$dir" "public/$dir"

    echo ' [DONE]'
done

# Convert index page
adoc2html "README.adoc"
mv public/README.html public/index.html

